import $ from 'jquery'

module.exports = {
    createUser: function (firstName, lastName, email, password, callback) {
        let data = {
            firstName: firstName,
            lastName: lastName,
            email: email,
            password: password
        }
        this.sendRequest('POST', '/auth/user', data, (err, data) => {
            // @TODO: implement actual logic and update with correct URL
            return err === data
        })
    },
    createPassword: function (passwordItem, callback) {
        this.sendRequest('POST', '/password', passwordItem, (err, data) => {
            if (err) {
                callback(err, null)
            } else {
                callback(err, data)
            }
        })
    },

    sendRequest (type, url, data, callback, contentType, async) {
        let request = {
            type: type,
            url: url,
            async: async,
            contentType: contentType || 'application/json; charset=utf-8',
            dataType: 'json',
            success: data => {
                if (callback) callback(null, data)
            },
            error: data => {
                if (!('error' in data)) {
                    data['error'] = 'Something went wrong'
                }

                if (callback) callback(data)
            }
        }

        if (data) {
            request['data'] = type === 'POST' ? JSON.stringify(data) : data
        }

        // @TODO: Sign in feature
        //
        // if (Session.loggedIn() || Session.getToken()) {
        //     request['beforeSend'] = xhr => {xhr.setRequestHeader('x-access-token', Session.getToken())}
        // }

        $.ajax(request)
    }
}
